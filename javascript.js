let btnScrollTop = $("#scrollToTop");
let userScroll = window.innerHeight;
let btnToggle = $("#toggle-btn");
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > userScroll) $("#scrollToTop").fadeIn();
        else $("#scrollToTop").fadeOut();
    })
    btnScrollTop.on("click", function () {
        $("body,html").animate({
            "scrollTop": "0"
        }, 1500);
        return false
    });
    $(`a[href^="#"]`).bind("click.smoothscroll", function (e) {
        e.preventDefault();
        let target = this.hash,
            $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 1200, 'swing', function () {
            window.location.hash = target;
        });
    })
});
$(document).ready(function(){
      btnToggle.click(function(){
          $(".second-bg").slideToggle(1000,function(){
              if($(this).is(":hidden")){
                  btnToggle.html("Показать секцию");
              }else{
                  btnToggle.html("Скрыть секцию");
              }
          });
          return fasle;
      })
})